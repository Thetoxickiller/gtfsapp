package lucas.com.gtfsapp;

import com.google.transit.realtime.GtfsRealtime;
import com.google.transit.realtime.GtfsRealtime.VehiclePosition;

/**
 * Created by lucas on 2018-05-18.
 */

public class Bus{
    public Trip trip;
    public int capacity = 20;
    public int population;
    VehiclePosition vehicle;

    public Bus(VehiclePosition v)
    {
        this.vehicle = v;
    }
}
