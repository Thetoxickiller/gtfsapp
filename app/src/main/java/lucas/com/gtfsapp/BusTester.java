package lucas.com.gtfsapp;

import com.google.transit.realtime.GtfsRealtime;

public class BusTester {

    public static boolean isOnBus(GPSData userLocation, float busLat, float busLong) {
        if(distFrom((float)userLocation.latitude, (float)userLocation.longitude, busLat, busLong) < 10){
            System.out.println("isOnBus: true");
            return true;
        }
        else {
            System.out.println("isOnBus: false");
            return false;
        }
    }

    public static float distFrom(float lat1, float lng1, float lat2, float lng2) {
        double earthRadius = 6371000; //meters
        double dLat = Math.toRadians(lat2-lat1);
        double dLng = Math.toRadians(lng2-lng1);
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                        Math.sin(dLng/2) * Math.sin(dLng/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        float dist = (float) (earthRadius * c);

        return dist;
    }

}
