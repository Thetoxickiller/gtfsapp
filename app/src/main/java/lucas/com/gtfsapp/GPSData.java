package lucas.com.gtfsapp;

/**
 * Created by lucas on 2018-05-18.
 */

public class GPSData {
    public double latitude;
    public double longitude;

    public GPSData(double longitude, double latitude)
    {
        this.longitude = longitude;
        this.latitude = latitude;
    }
}
