package lucas.com.gtfsapp;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;

public class GPSHandler {

    public static LocationManager locationManager;
    public static CustomLocationListener locationListener;

    public GPSHandler() {
        ActivityCompat.requestPermissions(MainActivity.instance, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
    }

    public void initGPS() {
        locationManager = (LocationManager)
                MainActivity.instance.getSystemService(Context.LOCATION_SERVICE);
        this.locationListener = new CustomLocationListener();

        System.out.println("GPSHANDLER CREATED");

        Thread thread = new Thread() {
            @Override
            public void run() {

                while(update()) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

            }
        };

        thread.start();
    }

    public GPSData get_gps_data()
    {
        if (ActivityCompat.checkSelfPermission(MainActivity.instance, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(MainActivity.instance, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            System.out.println("GPSHANDLER PERMISSION ERROR");
            //return;
            //MainActivity.instance.requestPermissions(new String[]{"ACCESS_FINE_LOCATION"}, 2);
            return null;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 10, this.locationListener);
        return locationListener.gpsData_recent;
    }

    public boolean update()
    {
        //System.out.println("dhadhakhda");
        return true;
    }


}