package lucas.com.gtfsapp;

import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import com.google.transit.realtime.GtfsRealtime;

import junit.framework.Test;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import static android.content.ContentValues.TAG;

public class GTFSFeed extends AsyncTask<Void, Void, Boolean> {

    private String urlLink;


    @Override
    protected void onPreExecute() {
        urlLink = "https://www.miapp.ca/GTFS_RT/Vehicle/VehiclePositions.pb";
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        try {
            Log.i("ssssstt", "adajdja");
            URL url = new URL("https://www.miapp.ca/GTFS_RT/Vehicle/VehiclePositions.pb");
            GtfsRealtime.FeedMessage feed = GtfsRealtime.FeedMessage.parseFrom(url.openStream());
            Log.i("eeeeeeeee", "adajdja");

            MainActivity.busManager.buses = new Bus[feed.getEntityCount()];

            int i = 0;

            for (GtfsRealtime.FeedEntity entity : feed.getEntityList()) {
                MainActivity.busManager.buses[i] = new Bus(entity.getVehicle());
                TestPopulation.initPopulation();
                for(int e = 0; e < TestPopulation.population.length; e++) {
                    //If the user is on a bus, increment that bus's population count
                    if (MainActivity.busTester.isOnBus(TestPopulation.population[e], entity.getVehicle().getPosition().getLatitude(), entity.getVehicle().getPosition().getLongitude())) {
                        MainActivity.busManager.buses[i].population++;
                    }
                }

                System.out.println("pop " + MainActivity.busManager.buses[0].population);
                break;
                //i++;

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }
    @Override
    protected void onPostExecute(Boolean success) {

    }


}