package lucas.com.gtfsapp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class GTFSParser {

    public Stop[] ParsedStops() {


        InputStream iS = null;

        try {
            iS = MainActivity.assets.open("stops.txt");
        } catch (IOException e) {
            e.printStackTrace();
        }

        String data = convertStreamToString(iS);

        //Split stop datafile by newline
        String[] initDataArray = data.split("\\r?\\n");

        //Create an array with one stop entry for each line in the datafile
        Stop[] Stops = new Stop[initDataArray.length];

        //Iterate through array of stop entries, finding 3rd entry in the list, which is position in Latitude and Longitude
        for (int i = 0; i < initDataArray.length; i++) {
            String[] separatedDataArray;
            //SeparatedDataArray Formatting: 0 = stop id, 2 = latlong position, rest is unimportant
            separatedDataArray = initDataArray[i].split(",,");
           // System.out.println(i);
            //System.out.println(initDataArray[i]);
            try {
                Stops[i] = new Stop(separatedDataArray[2].split(","), (separatedDataArray[0]));
              //  System.out.println(Stops[i].id);
                //System.out.println(Stops[i].position[0]);
                //System.out.println(Stops[i].position[1]);
            } catch (IndexOutOfBoundsException e) {
                e.printStackTrace();
            }
        }
        //System.out.println("len : " + initDataArray.length);
        return Stops;

    }

    public Trip[] ParsedTrips()
    {
        InputStream iS = null;

        try {
            iS = MainActivity.assets.open("trips.txt");
        } catch (IOException e) {
            e.printStackTrace();
        }

        String data = convertStreamToString(iS);

        //Split stop datafile by newline
        String[] initDataArray = data.split("\\r?\\n");

        //Create an array with one stop entry for each line in the datafile
        Trip[] trips = new Trip[initDataArray.length];

        //Iterate through array of stop entries, finding 3rd entry in the list, which is position in Latitude and Longitude
        for (int i = 1; i < initDataArray.length; i++) {
            String[] separatedDataArray;
            //SeparatedDataArray Formatting: 0 = stop id, 2 = latlong position, rest is unimportant
            separatedDataArray = initDataArray[i].split(",");
            System.out.println(separatedDataArray[0]);
            // System.out.println(i);
            //System.out.println(initDataArray[i]);
            try {
                trips[i - 1] = new Trip(Integer.parseInt(separatedDataArray[0]), Integer.parseInt(separatedDataArray[2]), separatedDataArray[5]);
                //  System.out.println(Stops[i].id);
                //System.out.println(Stops[i].position[0]);
                //System.out.println(Stops[i].position[1]);
            } catch (IndexOutOfBoundsException e) {
                e.printStackTrace();
            }
        }
        //System.out.println("len : " + initDataArray.length);
        System.out.println("aaa" + trips[0].route_id);
        return trips;
    }

    private String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line).append('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
}
