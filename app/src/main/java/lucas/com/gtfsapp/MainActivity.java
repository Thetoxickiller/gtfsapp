package lucas.com.gtfsapp;

import android.content.res.AssetManager;
import android.support.v7.app.AppCompatActivity;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.content.Context;
import android.location.LocationManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import java.net.URL;

public class MainActivity extends AppCompatActivity {
    static AssetManager assets;

    public static MainActivity instance;
    public GPSHandler gpsHandler;

    public static BusManager busManager = new BusManager();
    public static BusTester busTester = new BusTester();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        assets = getAssets();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.i("11111111", "adajdja");
        /*try {
            Log.i("ssssstt", "adajdja");
            URL url = new URL("https://www.miapp.ca/GTFS_RT/Vehicle/VehiclePositions.pb");
            GtfsRealtime.FeedMessage feed = GtfsRealtime.FeedMessage.parseFrom(url.openStream());
            Log.i("eeeeeeeee", "adajdja");
            for (GtfsRealtime.FeedEntity entity : feed.getEntityList()) {
                System.out.println("bibibibibib");
                if (entity.hasTripUpdate()) {
                    System.out.println("hihihihihihi " + entity.getTripUpdate());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }*/
        new GTFSFeed().execute();
        new GTFSParser().ParsedStops();
        new GTFSParser().ParsedTrips();
       // new GTFSFeed().execute();

        instance = this;

        gpsHandler = new GPSHandler();

        BusTester.isOnBus(new GPSData(1, 1), 3 ,3);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults)
    {
        gpsHandler.initGPS();
    }

}
