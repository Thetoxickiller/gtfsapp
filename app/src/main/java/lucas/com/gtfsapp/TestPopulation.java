package lucas.com.gtfsapp;

import java.util.Random;

/**
 * Created by lucas on 2018-05-18.
 */

public class TestPopulation {
    public static GPSData[] population = new GPSData[1000];
    public static void initPopulation()
    {
        for(int i = 0; i < population.length; i++)
        {
            population[i] = new GPSData(new Random().nextInt(400)/10000 + MainActivity.busManager.buses[0].vehicle.getPosition().getLongitude(),
                    new Random().nextInt(100)/10000 + MainActivity.busManager.buses[0].vehicle.getPosition().getLatitude());
            System.out.println("pop" + population[i]);
        }
    }

}
