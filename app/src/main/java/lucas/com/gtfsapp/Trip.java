package lucas.com.gtfsapp;

/**
 * Created by lucas on 2018-05-18.
 */

public class Trip {
    public int route_id;
    public int trip_id;
    public String direction_id;

    public Trip(int route_id, int trip_id, String direction_id)
    {
        this.route_id = route_id;
        this.trip_id = trip_id;
        this.direction_id = direction_id;
    }
}
